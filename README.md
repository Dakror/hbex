# Huawei Backup Extractor

Extract all the files from a Huawei Backup database into their original files and folders.

## Usage

```hbex.jar <db-path> <output-path>```

## Dependencies

*  sqlite-jdbc