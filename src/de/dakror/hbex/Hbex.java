/*******************************************************************************
 * Copyright 2018 Maximilian Stark | Dakror <mail@dakror.de>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package de.dakror.hbex;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Maximilian Stark | Dakror
 */
public class Hbex {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        if (args.length < 2) {
            System.err.println("Usage: hbex db-path output-path");
            return;
        }

        File f = new File(args[0]);
        if (!f.exists()) {
            System.err.println("Invalid backup db file");
            return;
        }

        File out = new File(args[1] + "/" + f.getName().substring(0, f.getName().lastIndexOf(".")));
        if (!out.mkdirs()) {
            System.err.println("Invalid output dir");
            return;
        }

        Class.forName("org.sqlite.JDBC");
        Connection con = DriverManager.getConnection("jdbc:sqlite:" + f.getPath());

        ResultSet rs = con.createStatement().executeQuery("SELECT * " +
                "FROM apk_file_info i, apk_file_data d " +
                "WHERE i.file_index = d.file_index " +
                "ORDER BY i.file_index ASC, d.data_index ASC ");

        FileOutputStream fos = null;
        String curFile = null;
        String cur_file_path = null;

        while (rs.next()) {
            String file_path = rs.getString(2);

            if (fos == null || !file_path.equals(cur_file_path)) {
                if (fos != null) {
                    try {
                        System.out.println("Written " + cur_file_path);
                        fos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                cur_file_path = file_path;
                curFile = out.getPath() + file_path;
                new File(curFile).getParentFile().mkdirs();
                try {
                    fos = new FileOutputStream(curFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            byte[] data = rs.getBytes(10);
            try {
                fos.write(data, 0, data.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
